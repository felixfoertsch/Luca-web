export const FAQ_LINK = 'https://www.luca-app.de/faq/';
export const VIDEOS_LINK = 'https://youtu.be/aYj9hyldumM';
export const TOOLKIT_LINK = 'https://www.luca-app.de/download/';
export const GITLAB_LINK =
  'https://gitlab.com/lucaapp/web/-/tree/master/services/locations';
export const TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions/';
