export const E2E_EMAIL = 'e2e@nexenio.com';
export const E2E_PASSWORD = 'e2eTesting!';

export const E2E_PHONE_NUMBER = '+4917612345678';

export const E2E_FIRSTNAME = 'Torsten';
export const E2E_LASTNAME = 'Tester';

export const E2E_LOCATION_PRIVATE_KEY_PATH =
  './downloads/luca_locations_Torsten_Tester_privateKey.luca';

export const E2E_LOCATION_PRIVATE_KEY_NAME =
  'luca_locations_Torsten_Tester_privateKey.luca';

export const E2E_LOCATION_WRONG_PRIVATE_KEY_PATH =
  './specs/locations/assets/luca_locations_wrong_privateKey.luca';

export const E2E_LOCATION_WRONG_PRIVATE_KEY_NAME =
  'luca_locations_wrong_privateKey.luca';
