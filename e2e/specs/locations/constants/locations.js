export const E2E_DEFAULT_LOCATION_GROUP =
  'c951f526-f792-498b-838f-7d1312a792a1';
export const E2E_DEFAULT_LOCATION_GROUP_2 =
  'c951f526-f792-498b-838f-7d1312a792a2';

export const E2E_DEFAULT_GROUP_NAME = 'Nexenio_1 e2e';
export const E2E_GROUP_NAME_2 = 'Nexenio_2 e2e';

export const E2E_DEFAULT_LOCATION_UUID = 'c951f526-f792-498b-838f-7d1312a792a0';
export const E2E_DEFAULT_LOCATION_NAME = 'General';

export const E2E_SECOND_LOCATION_UUID = 'c951f526-f792-498b-838f-7d1312a792a3';
export const E2E_SECOND_LOCATION_NAME = 'Restaurant';

export const E2E_THIRD_LOCATION_UUID = '04d3e0b3-c64f-43bd-9b1a-f53f9032e312';
export const E2E_THIRD_LOCATION_NAME = 'Nexenio Kitchen';

export const E2E_DEFAULT_LOCATION_FORM = '68e580d5-3921-48ce-b8ad-b313ec28926f';
export const E2E_DEFAULT_LOCATION_SCANNER =
  '09eb8d41-1914-4950-9526-36ebc6ad58fd';
export const E2E_DEFAULT_SCANNER_LINK = `https://localhost/scanner/${E2E_DEFAULT_LOCATION_SCANNER}`;
export const E2E_DEFAULT_CAM_SCANNER_LINK = `https://localhost/scanner/cam/${E2E_DEFAULT_LOCATION_SCANNER}`;
export const E2E_DEFAULT_CONTACT_FORM_LINK = `https://localhost/contact-form/${E2E_DEFAULT_LOCATION_SCANNER}`;

export const NEW_ROOM_LOCATION = 'NEW_ROOM_LOCATION';
export const NEW_BASE_LOCATION = 'NEW_BASE_LOCATION';
export const NEW_BUILDING_LOCATION = 'NEW_BUILDING_LOCATION';
export const NEW_RESTAURANT_LOCATION = 'NEW_RESTAURANT_LOCATION';

export const ROOM_TYPE = 'room';
export const BASE_TYPE = 'base';
export const BUILDING_TYPE = 'building';
export const RESTAURANT_TYPE = 'restaurant';

export const TEST_NUMBER = '0176 1111 1111';
export const TEST_LOCATION_NAME = 'Test_Restaurant';

export const RESTAURANT_NAME = 'Test Restaurant';
export const RESTAURANT_ADDRESS = 'Nexenio';
export const RESTAURANT_PHONE = '017601234567';
export const RESTAURANT_TABLE_COUNT = '12';
export const RESTAURANT_RADIUS = '100';
