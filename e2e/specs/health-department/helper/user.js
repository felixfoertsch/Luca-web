export const E2E_HEALTH_DEPARTMENT_PASSWORD = 'testing';
export const E2E_HEALTH_DEPARTMENT_USERNAME = 'luca@nexenio.com';
export const HEALTH_DEPARTMENT_PRIVATE_KEY_PATH =
  './downloads/HealthDepartmentKeyFile.luca';
export const HEALTH_DEPARTMENT_WRONG_PRIVATE_KEY_PATH =
  './assets/HealthDepartmentWrongKeyFile.luca';
export const HEALTH_DEPARTMENT_WRONG_FILE_PRIVATE_KEY_PATH =
  './assets/dummy.pdf';
export const HEALTH_DEPARTMENT_LARGE_SIZE_FILE_PRIVATE_KEY_PATH =
  './assets/HealthDepartmentLargeFile.luca';
