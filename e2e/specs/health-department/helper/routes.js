export const HEALTH_DEPARTMENT_BASE_ROUTE = 'https://localhost:9443/health-department';
export const HEALTH_DEPARTMENT_APP_ROUTE = `${HEALTH_DEPARTMENT_BASE_ROUTE}/app`;
export const HEALTH_DEPARTMENT_TRACKING_ROUTE = '/app/tracking';
export const HEALTH_DEPARTMENT_USER_MANAGEMENT_ROUTE = `${HEALTH_DEPARTMENT_BASE_ROUTE}/app/user-management`;
