// import forge, { pkcs12 } from 'node-forge';
var forge = require('node-forge');
var pkcs12 = forge.pkcs12
var jwt = require('jsonwebtoken');

const JWT_ALGORITHM = 'RS512';
const PASSWORD = 'testing';

const decryptPkcs12 = p12file => {
  const p12Asn1 = forge.asn1.fromDer(p12file);
  const p12 = forge.pkcs12.pkcs12FromAsn1(p12Asn1, PASSWORD);

  const certBags = p12.getBags({ bagType: forge.pki.oids.certBag })[
    forge.pki.oids.certBag
  ];

  if (!certBags || certBags.length === 0) {
    throw new Error('Certificate not found');
  }

  const privateKeyBags = p12.getBags({
    bagType: forge.pki.oids.pkcs8ShroudedKeyBag,
  })[forge.pki.oids.pkcs8ShroudedKeyBag];

  if (!privateKeyBags || privateKeyBags.length === 0) {
    throw new Error('Private Key not found');
  }

  const certBag = certBags[0];
  const privateKeyBag = privateKeyBags[0];

  if (!certBag.cert) {
    throw new Error('Certificate not found');
  }

  if (!privateKeyBag.key) {
    throw new Error('Private key not found');
  }

  const certDer = forge.asn1.toDer(forge.pki.certificateToAsn1(certBag.cert));
  const md = forge.md.sha1.create();
  md.update(certDer.data);
  const fingerprint = md.digest().toHex();

  const commonName = certBag.cert.subject.getField('CN')?.value;
  const serialName = certBag.cert.subject.getField({ name: 'serialName' })
    ?.value;

  const cert = forge.pki.certificateToPem(certBag.cert);
  const publicKey = forge.pki.publicKeyToPem(certBag.cert.publicKey);
  const privateKey = forge.pki.privateKeyToPem(privateKeyBag.key);
  return {
    cert: cert.replace(/\r\n/g, '\n'),
    publicKey: publicKey.replace(/\r\n/g, '\n'),
    privateKey: privateKey.replace(/\r\n/g, '\n'),
    fingerprint,
    commonName,
    serialName,
  };
};

const createSignedPublicKeys = (healthDepartment, p12file) => {
  const pkcs12 = decryptPkcs12(p12file);

  const signedPublicHDSKP = jwt.sign(
    {
      sub: healthDepartment.uuid, // health department uuid
      iss: pkcs12.fingerprint, // sha1 fingerprint of cert (hex)
      name: healthDepartment.name, // name of the health department
      key: healthDepartment.publicHDSKP, // public key (base64)
      type: 'publicHDSKP', // key type (base64)
    },
    pkcs12.privateKey,
    { algorithm: JWT_ALGORITHM }
  );

  const signedPublicHDEKP = jwt.sign(
    {
      sub: healthDepartment.uuid, // health department uuid
      iss: pkcs12.fingerprint, // sha1 fingerprint of cert (hex)
      name: healthDepartment.name, // name of the health department
      key: healthDepartment.publicHDEKP, // public key (base64)
      type: 'publicHDEKP', // key type (base64)
    },
    pkcs12.privateKey,
    { algorithm: JWT_ALGORITHM }
  );

  return {
    publicCertificate: pkcs12.cert,
    signedPublicHDSKP,
    signedPublicHDEKP,
  };
};

const healthDepartment = {
  "uuid": "d347b793-a3ab-40fb-acc6-9c7d572e67d5",
  "name": "neXenio Testing",
  "commonName": "Dev Health Department",
  "publicHDEKP": "BETh7U2w6tbW6tyHto7oPln5CGQKsZ6wy5nj+MBMFK6wD0BGr2EBTIKIFhdQO3jvJ0ievBIWzDKdcjPzjgdx9Bs=",
  "publicHDSKP": "BCNdOvSJ5eV//V3GIQhNm/lZtFlKKcZRROarx8foZFPCp3KA17kFdHp5gDQoeJxL+uWJpBLUqjtVyAu78Pc2+rY=",
  "publicCertificate": null,
  "signedPublicHDEKP": null,
  "signedPublicHDSKP": null,
  "email": null,
  "phone": null
};

var fs = require('fs');
const pkcs12binary = fs.readFileSync('../../../certs/health.pfx', { encoding: "binary" });

const signedPublicKeys = createSignedPublicKeys(
  healthDepartment,
  pkcs12binary
);

console.log(signedPublicKeys);
