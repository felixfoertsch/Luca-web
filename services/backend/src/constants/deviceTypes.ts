export const DEVICE_TYPE_IOS = 0;
export const DEVICE_TYPE_ANDROID = 1;
export const DEVICE_TYPE_STATIC = 2;
export const DEVICE_TYPE_WEBAPP = 3;
export const DEVICE_TYPE_FORM = 4;
