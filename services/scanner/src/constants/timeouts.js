export const SCAN_TIMEOUT = 700;

// 5 min in ms
export const REFETCH_INTERVAL_MS = 300000;
