export const TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions/';

export const GITLAB_LINK =
  'https://gitlab.com/lucaapp/web/-/tree/master/services/contact-form';

export const PRIVACY_LINK =
  'https://www.luca-app.de/contact-form-privacy-policy/';
