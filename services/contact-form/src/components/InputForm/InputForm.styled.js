import styled from 'styled-components';

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 40px;
`;

export const Link = styled.a`
  color: rgb(78, 97, 128);
  &:hover {
    color: rgb(151, 165, 187);
  }
`;
