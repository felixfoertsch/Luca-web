import styled from 'styled-components';

export const GitlabWrapper = styled.div`
  padding: 10px;
`;

export const GitLabLink = styled.a`
  color: rgb(255, 255, 255);
  font-size: 14px;
  text-decoration: underline;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 16px;
`;
