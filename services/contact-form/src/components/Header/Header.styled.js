import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  padding: 40px;
  flex-basis: 20%;
`;

export const LinksWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 40px;
`;

export const Logo = styled.img`
  height: 48px;
`;
