export const TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions/';

export const FAQ_LINK = 'https://www.luca-app.de/faq/';
export const VIDEOS_LINK = 'https://youtu.be/aYj9hyldumM';
export const TOOLKIT_LINK = 'https://www.luca-app.de/download/';

export const QR_PRINT_LINK = 'https://www.qr-print.de/luca';
export const GITLAB_LINK =
  'https://gitlab.com/lucaapp/web/-/tree/master/services/locations';

export const REGISTER_BADGE_PRIVACY_LINK =
  'https://www.luca-app.de/badge-privacy-policy/';

export const WEB_APP_BASE_PATH = `https://${window.location.host}/webapp/`;

export const SUPPORT_EMAIL = 'datenfreigabe@luca-app.de';
