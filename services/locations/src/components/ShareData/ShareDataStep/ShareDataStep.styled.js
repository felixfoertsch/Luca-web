import styled from 'styled-components';

export const InfoText = styled.div`
  font-size: 16px;
  margin-bottom: 24px;
`;

export const StyledLabel = styled.div`
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 8px;
`;

export const StyledValue = styled.div`
  font-size: 16px;
  font-weight: bold;
  display: flex;
  align-items: center;
`;

export const StyledTransfer = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 24px;
`;

export const StyledHealthDepartment = styled.div`
  display: flex;
  align-items: center;
`;
