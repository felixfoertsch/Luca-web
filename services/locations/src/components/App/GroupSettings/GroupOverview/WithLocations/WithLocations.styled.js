import styled from 'styled-components';

export const Counter = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 34px;
  font-weight: 500;
`;
