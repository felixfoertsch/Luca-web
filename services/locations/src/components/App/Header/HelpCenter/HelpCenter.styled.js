import styled from 'styled-components';

export const HelpCenterComp = styled.div`
  margin-right: 24px;
  cursor: pointer;
  display: inline-block;
`;
