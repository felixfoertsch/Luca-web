import styled from 'styled-components';
import { Menu } from 'antd';
import Icon from '@ant-design/icons';

export const StyledMenuItem = styled(Menu.Item)`
  padding: 12px 32px;
`;

export const StyledIcon = styled(Icon)`
  font-size: 32px;
`;
