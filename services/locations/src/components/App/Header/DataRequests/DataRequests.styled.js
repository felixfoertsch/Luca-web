import styled from 'styled-components';

export const DataRequestsComp = styled.div`
  margin-right: 24px;
  cursor: pointer;
  display: inline-block;
`;

export const badgeStyle = {
  right: 24,
  backgroundColor: 'rgb(239, 92, 5)',
};
