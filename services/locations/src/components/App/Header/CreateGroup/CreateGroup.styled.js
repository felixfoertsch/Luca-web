import styled from 'styled-components';

export const CreateGroupComp = styled.div`
  margin-right: 24px;
  cursor: pointer;
  display: inline-block;
`;
