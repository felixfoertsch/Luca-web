import styled from 'styled-components';

export const DevicesComp = styled.div`
  margin-right: 24px;
  cursor: pointer;
  display: inline-block;
`;
