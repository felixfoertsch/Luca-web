import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  border-right: 1px solid rgb(151, 151, 151);
`;

export const Counter = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 34px;
  font-weight: 500;
  margin-right: 16px;
`;

export const Refresh = styled.div`
  color: rgb(80, 102, 124);
  cursor: pointer;
  margin: 0 16px 0 32px;
`;
