import styled from 'styled-components';

export const Description = styled.div`
  margin-bottom: 24px;
  display: flex;
`;

export const InfoComp = styled.div`
  cursor: pointer;
  margin-left: 16px;
  color: rgba(0, 0, 0, 0.87);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 12px;
  font-weight: bold;
  align-self: flex-end;
  margin-bottom: 1px;
`;
