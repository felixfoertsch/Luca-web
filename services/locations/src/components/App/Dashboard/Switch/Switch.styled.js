import styled from 'styled-components';

export const StyledInfo = styled.div`
  font-size: 12px;
  font-weight: 500;
  text-align: right;
  padding-right: 8px;
  color: rgba(0, 0, 0, 0.87);
`;
export const StyledContainer = styled.div`
  display: flex;
  align-items: center;
`;
