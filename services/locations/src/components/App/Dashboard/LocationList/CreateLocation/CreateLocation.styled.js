import styled from 'styled-components';

export const Wrapper = styled.div`
  position: sticky;
  bottom: 0;
  display: flex;
  padding: 24px 0 20px 42px;
  cursor: pointer;
  background-color: #f3f5f7;
`;

export const CreateText = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  margin-left: 8px;
`;

export const iconStyle = {
  fontSize: 10,
  backgroundColor: 'white',
  borderRadius: '50%',
  padding: 6,
};
