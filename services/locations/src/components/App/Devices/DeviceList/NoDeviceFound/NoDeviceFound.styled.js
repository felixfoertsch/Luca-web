import styled from 'styled-components';

export const NoDeviceFoundWrapper = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`;
export const NoDeviceFoundHeadline = styled.div`
  width: 100%;
  font-size: 22px;
  font-weight: bold;
  line-height: 20px;
  text-align: center;
  color: rgb(0, 0, 0);
  padding: 0 32px 16px;
  font-family: Montserrat-SemiBold, sans-serif;
`;
export const NoDeviceFoundInfo = styled.div`
  width: 100%;
  font-size: 18px;
  line-height: 20px;
  text-align: center;
  color: rgb(0, 0, 0);
  padding: 0 32px 32px;
  font-family: Montserrat-SemiBold, sans-serif;
`;
