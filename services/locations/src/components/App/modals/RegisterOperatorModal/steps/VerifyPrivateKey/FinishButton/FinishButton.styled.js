import styled from 'styled-components';

export const FinishButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ align }) => align};
  margin-top: 40px;
`;
