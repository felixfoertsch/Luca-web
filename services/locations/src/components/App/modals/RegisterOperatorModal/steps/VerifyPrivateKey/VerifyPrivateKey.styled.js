import styled from 'styled-components';

export const InfoBlock = styled.p`
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const RequestContent = styled.div`
  margin-bottom: 40px;
`;

export const UploadMessage = styled.p`
  font-size: 14px;
  font-weight: 500;
  text-align: center;
`;

export const UploadProgress = styled.div`
  display: flex;
  width: 200px;
`;
