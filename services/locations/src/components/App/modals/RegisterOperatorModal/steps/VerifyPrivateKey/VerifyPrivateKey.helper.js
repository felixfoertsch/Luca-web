export const uploadMessages = {
  initial: 'shareData.privateKey.uploadMessage',
  inProgress: 'shareData.privateKey.uploadAction',
  size: 'shareData.privateKey.keySize',
  error: 'shareData.privateKey.errorMessage',
  done: 'shareData.privateKey.done',
};

export const statusProgress = {
  initial: '',
  success: 'success',
  exception: 'exception',
};
