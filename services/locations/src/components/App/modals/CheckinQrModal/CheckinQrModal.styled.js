import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Description = styled.div`
  margin-bottom: 40px;
`;

export const QrCodeWrapper = styled.div`
  margin-bottom: 40px;
  width: 100%;
  text-align: center;
`;
