import styled from 'styled-components';
import { PrimaryButton } from 'components/general';

export const GooglePlacesPrimaryButton = styled(PrimaryButton)`
  margin-right: 24px;
`;
