import styled from 'styled-components';

export const Error = styled.div`
  color: #ff4d4f;
  font-size: 14px;
  font-weight: 500;
`;
