import styled from 'styled-components';

export const ForgotPasswordLinkWrapper = styled.div`
  text-decoration: underline;
  display: inline-block;
  text-align: left;
  width: 100%;
`;
