export const TERMS_CONDITIONS_LINK =
  'https://luca-app.de/app-terms-and-conditions/';

export const PRIVACY_LINK = 'https://www.luca-app.de/webapp-privacy-policy/';

export const SHARE_DATA_INFO_LINK =
  'https://www.luca-app.de/app-privacy-policy/#data_privacy_app_F_9/';
