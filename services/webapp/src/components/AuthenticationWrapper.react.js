import { useCallback, useEffect, useMemo, useState } from 'react';
import { notification } from 'antd';
import { useIntl } from 'react-intl';
import useInterval from 'use-interval';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getLocation } from 'connected-react-router';
import { indexDB } from 'db';
import {
  APPOINTMENT_PATH,
  BASE_PRIVATE_MEETING_PATH,
  CHECK_OUT_PATH,
  COVID_TEST_PATH,
  HOME_PATH,
  ON_BOARDING_PATH,
} from 'constants/routes';
import {
  checkHistory,
  checkLocalHistory,
  checkSession,
  syncHistory,
} from 'helpers/history';
import { getCheckOutPath } from 'helpers/routes';
import { checkForActiveHostedPrivateMeeting } from 'helpers/privateMeeting';

export function AuthenticationWrapper({ children }) {
  const intl = useIntl();
  const history = useHistory();
  const location = useSelector(getLocation);
  const [isHostingMeeting, setIsHostingMeeting] = useState(false);

  const checkLocation = useCallback(
    appLocation => {
      (async () => {
        if ((await indexDB.users.count().catch(() => 0)) === 0) {
          if (
            !appLocation.pathname.includes(ON_BOARDING_PATH) &&
            !appLocation.pathname.includes(APPOINTMENT_PATH) &&
            !appLocation.pathname.includes(COVID_TEST_PATH)
          ) {
            if (appLocation.pathname.includes(HOME_PATH)) {
              const splits = appLocation.pathname.split('/');

              if (splits[splits.length - 1]) {
                history.push(
                  `${ON_BOARDING_PATH}/${
                    splits[splits.length - 1]
                  }?isPrivateMeeting=${appLocation.pathname.includes(
                    BASE_PRIVATE_MEETING_PATH
                  )}${appLocation.hash}`
                );
                return;
              }
            }

            history.push(ON_BOARDING_PATH);
          }
          return;
        }

        if (appLocation.pathname.includes(ON_BOARDING_PATH)) {
          history.push(HOME_PATH);
        }

        try {
          const activeMeeting = await checkForActiveHostedPrivateMeeting();
          if (activeMeeting) {
            setIsHostingMeeting(true);
            if (!appLocation.pathname.includes(BASE_PRIVATE_MEETING_PATH)) {
              history.push(BASE_PRIVATE_MEETING_PATH);
            }

            return;
          }
        } catch (error) {
          console.error(error);
        }

        setIsHostingMeeting(false);

        const openSession = await checkLocalHistory();
        if (openSession && !location.pathname.includes(CHECK_OUT_PATH)) {
          history.push(getCheckOutPath(openSession.traceId));
          return;
        }

        let isCheckedIn = false;
        const sessions = await checkHistory();
        const historySync = [];
        for (const session of sessions) {
          historySync.push(syncHistory(session));

          if (
            !isCheckedIn &&
            !session.checkout &&
            !location.pathname.includes(CHECK_OUT_PATH)
          ) {
            isCheckedIn = true;
            history.push(getCheckOutPath(session.traceId));
          }
        }
        await Promise.all(historySync);
      })().catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'error.headline',
          }),
          description: intl.formatMessage({
            id: 'error.description',
          }),
        });
      });
    },
    [history, intl, location.pathname]
  );

  /**
   * Checks if the user has checked in by having their qr code scanned. This is done by
   * sending the recent traceIds to the server to check if any has been registered.
   *
   * @see https://www.luca-app.de/securityoverview/processes/guest_app_checkin.html#qr-code-scanning-feedback
   */
  const checkTraces = useMemo(() => {
    return async () => {
      if (isHostingMeeting) {
        return;
      }

      const searchParameters = new URLSearchParams(location.search);
      const traceId = searchParameters.get('traceId');

      try {
        if (traceId && location.pathname.includes(CHECK_OUT_PATH)) {
          const historyEntry = await checkSession(traceId);
          if (historyEntry.checkout) {
            history.push(HOME_PATH);
          }
          return;
        }
      } catch {
        notification.error({
          message: intl.formatMessage({
            id: 'error.headline',
          }),
          description: intl.formatMessage({
            id: 'error.description',
          }),
        });
      }

      const sessions = await checkHistory(10);
      if (!sessions) return;

      for (const session of sessions) {
        if (!session.checkout && !location.pathname.includes(CHECK_OUT_PATH)) {
          history.push(getCheckOutPath(session.traceId));
          return;
        }
      }
    };
  }, [history, intl, isHostingMeeting, location.pathname, location.search]);

  useInterval(checkTraces, 800);

  useEffect(() => {
    checkTraces();
  }, [checkTraces]);

  useEffect(() => {
    checkLocation(location);
  }, [checkLocation, location]);

  return children;
}
