import React from 'react';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';
import { ConnectedRouter } from 'connected-react-router';
import { QueryClient, QueryClientProvider } from 'react-query';

import 'moment/locale/de';
import moment from 'moment';

import { IntlProvider } from 'react-intl';

import {
  HOME_PATH,
  HISTORY_PATH,
  SETTINGS_PATH,
  LICENSES_ROUTE,
  CHECK_OUT_PATH,
  APPOINTMENT_PATH,
  COVID_TEST_PATH,
  ON_BOARDING_PATH,
  BASE_PRIVATE_MEETING_PATH,
  CHECKIN_TO_PRIVATE_MEETING_PATH,
  EDIT_CONTACT_INFORMATION_SETTING,
} from 'constants/routes';

import { getLanguage } from 'utils/language';

import { ErrorWrapper } from 'components/ErrorWrapper';

import { Home } from 'components/Home';
import { History } from 'components/History';
import { Licenses } from 'components/Licenses';
import { Settings } from 'components/Settings';
import { CheckOut } from 'components/Checkout';
import { OnBoarding } from 'components/OnBoarding';
import { UserSessionProvider } from 'contexts/userSessionContext';
import { ContactInformation } from 'components/ContactInformation';
import { FeatureNotImplemented } from 'components/FeatureNotImplemented';
import { AuthenticationWrapper } from 'components/AuthenticationWrapper.react';
import { PrivateMeeting } from 'components/PrivateMeeting/PrivateMeeting.react';
import { UpdatedTermsAndConditionsWrapper } from 'components/UpdatedTermsAndConditionsWrapper';

import { configureStore } from 'configureStore';

import { AppWrapper } from './App.styled';

import { messages } from './messages';

const history = createBrowserHistory();
const store = configureStore(undefined, history);

moment.locale(getLanguage());
document.documentElement.lang = getLanguage();

const queryClient = new QueryClient();

export const Main = () => {
  return (
    <AppWrapper>
      <HelmetProvider>
        <Helmet>
          <meta name="apple-itunes-app" content="app-id=1531742708" />
        </Helmet>
        <Provider store={store}>
          <IntlProvider
            locale={getLanguage()}
            messages={messages[getLanguage()]}
            wrapRichTextChunksInFragment
          >
            <QueryClientProvider client={queryClient}>
              <ConnectedRouter history={history}>
                <ErrorWrapper>
                  <AuthenticationWrapper>
                    <UserSessionProvider>
                      <UpdatedTermsAndConditionsWrapper>
                        <Switch>
                          <Route
                            path={ON_BOARDING_PATH}
                            component={OnBoarding}
                          />
                          <Route path={LICENSES_ROUTE} component={Licenses} />
                          <Route
                            path={`${ON_BOARDING_PATH}/:scannerId/`}
                            component={OnBoarding}
                          />
                          <Route
                            path={`${CHECK_OUT_PATH}/`}
                            component={CheckOut}
                          />
                          <Route
                            path={EDIT_CONTACT_INFORMATION_SETTING}
                            component={ContactInformation}
                          />
                          <Route path={HISTORY_PATH} component={History} />
                          <Route path={SETTINGS_PATH} component={Settings} />
                          <Route
                            path={[APPOINTMENT_PATH, COVID_TEST_PATH]}
                            component={FeatureNotImplemented}
                          />
                          <Route
                            component={Home}
                            path={CHECKIN_TO_PRIVATE_MEETING_PATH}
                          />
                          <Route
                            path={BASE_PRIVATE_MEETING_PATH}
                            component={PrivateMeeting}
                          />
                          <Route
                            component={Home}
                            path={`${HOME_PATH}/:scannerId`}
                          />
                          <Route path={HOME_PATH} component={Home} />
                        </Switch>
                      </UpdatedTermsAndConditionsWrapper>
                    </UserSessionProvider>
                  </AuthenticationWrapper>
                </ErrorWrapper>
              </ConnectedRouter>
            </QueryClientProvider>
          </IntlProvider>
        </Provider>
      </HelmetProvider>
    </AppWrapper>
  );
};
