import styled from 'styled-components';

export const PageWrapper = styled.div`
  background-color: rgb(195, 206, 217);
  padding: 40px;
  height: 100vh;
`;

export const ContentWrapper = styled.div`
  background-color: white;
  display: flex;
  flex-direction: column;
  padding: 24px 32px 0 32px;
  height: 90vh;
  overflow-y: auto;
`;
