import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
export const Heading = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 16px;
`;
export const StatisticWrapper = styled.div`
  display: flex;
`;
export const Statistics = styled.div`
  font-size: 16px;
  font-weight: 500;
  margin-right: 40px;
`;
