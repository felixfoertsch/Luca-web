import styled from 'styled-components';

export const Expiry = styled.div`
  margin-top: 8px;
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-left: 16px;
`;

export const ButtonWrapper = styled.div`
  text-align: left;
`;

export const Wrapper = styled.div`
  display: flex;
`;
