import styled from 'styled-components';

export const Time = styled.div`
  width: 100%;
  flex-grow: 1;
  margin: auto 0;
`;

export const Contact = styled.div`
  width: 100%;
  flex-grow: 1;
  margin: auto 0;
`;
