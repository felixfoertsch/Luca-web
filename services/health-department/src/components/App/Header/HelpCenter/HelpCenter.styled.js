import styled from 'styled-components';

export const iconStyle = {
  fontSize: 16,
  color: '#50667c',
  alignSelf: 'flex-end',
};

export const HelpCenterComp = styled.div`
  margin-right: 24px;
  cursor: pointer;
  display: inline-block;
`;

export const MenuText = styled.span`
  padding: 0 16px;
`;
