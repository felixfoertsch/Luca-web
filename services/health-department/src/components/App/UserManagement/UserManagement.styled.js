import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 100%;
  padding: 32px 32px 0 32px;
  background-color: rgb(232, 231, 229);
  position: relative;
`;

export const VersionFooterWrapper = styled.div`
  padding-top: 24px;
  padding-bottom: 24px;
  float: right;
`;
