import styled from 'styled-components';

export const ModalWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
`;
