import styled from 'styled-components';

export const Heading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 24px;
  font-weight: bold;
  margin-top: 36px;
`;

export const Count = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin-top: 8px;
`;
