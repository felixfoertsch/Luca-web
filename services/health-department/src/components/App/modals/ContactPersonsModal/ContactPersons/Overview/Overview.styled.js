import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  margin-top: 40px;
`;

export const OverviewItem = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 60px;
`;

export const Name = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 8px;
`;

export const Value = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
`;
