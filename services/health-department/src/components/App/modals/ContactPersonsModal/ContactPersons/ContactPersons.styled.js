import styled from 'styled-components';

export const ContactPersonsWrapper = styled.div`
  padding-bottom: 32px;
`;

export const FlexWrapper = styled.div`
  display: flex;
`;
