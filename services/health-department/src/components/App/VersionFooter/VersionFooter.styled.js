import styled from 'styled-components';

export const FooterWrapper = styled.footer``;

export const InfoWrapper = styled.span`
  padding-right: 32px;
`;

export const GitlabLink = styled.a`
  color: black;
`;
