import styled from 'styled-components';

export const Wrapper = styled.div`
  padding-bottom: 32px;
  margin-bottom: 24px;
`;

export const ChildWrapper = styled.div`
  width: 65%;
`;

export const StyledHeadline = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const StyledText = styled.div`
  font-size: 14px;
  text-align: justify;
  font-weight: 500;
  margin-bottom: 40px;
`;

export const ButtonWrapper = styled.div`
  text-align: right;
`;

export const VersionTag = styled.div`
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 14px;
  font-weight: 600;
`;
