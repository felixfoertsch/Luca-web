import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 65%;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

export const StyledHeadline = styled.div`
  margin-bottom: 16px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
`;

export const StyledInfo = styled.div`
  margin-bottom: 16px;
  color: rgb(0, 0, 0);
  font-size: 14px;
`;
