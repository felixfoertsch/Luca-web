export const SUPPORTED_SORMAS_VERSIONS = {
  '"1.57.0"': true,
  '"1.59.1"': true,
  '"1.59.2"': true,
  '"1.61.1"': true,
  '"1.62.3"': true,
  '"1.63.1"': true,
};
